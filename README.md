# Pyhon and Linux Bash: Support Vector Machine #

## Repository Description ##
This repository contains our implementation of Support Vector Machine algorithm, which was done as a final project for one of our subjects: "Programming in scripting lanuages: Python and Linux Bash".

## Stack and development tools ##
- python 2.7
- virtualenv
- pip
- pyscaffold

## Authors ##
See AUTHORS.rst

## Version ##
See CHANGES.rst or use git repo to see changes.

## License ##
See LICENSE.txt


# Setup #
Project uses template created by pyscaffold. The easiest way of development or use is to clone git repository and prepare virtual environment for python. You may also operate on your system default python version and packages, but in this case you will have to change it or install additional packages what might result in conflicts between your python projects.
Assumming that you have git and any python installed:
````
$ git clone https://skurzepa@bitbucket.org/skurzepa/pyilb_svm.git
$ cd pyilb_svm
$ sudo apt-get install python-pip
$ pip install virtualenv
$ virtualenv -p /usr/bin/python2.7 venv
$ source venv/bin/activate
````
## for development or tests ##
````
$ pip install -r test-requirements.txt
````
## for use ##
````
$ pip install -r requirements.txt
````
End with:
````
$ deactivate
````


# Usage #
Because of working in virtual environment you have to turn it on before and turn it off after working. Assumming that we are in repo folder:
````
$ source venv/bin/activate
    ... working ...
$ deactivate
````


# Running tests #
You should run both tests and example from the main repo directory. Remember about turning on your virtualenv before!
````
$ python tests/test.py
````
````
$ python tests/example.py
````
