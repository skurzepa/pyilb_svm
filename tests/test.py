# import tools
import numpy as np
import time
import sys
import os
sys.path.insert(0, 'pyilb_svm/')

# import libraries
from sklearn import svm as lib
import svm as ours

print("PyiLB: SVM test\nSzymon Kurzepa, Maciej Jelen, Michal Magola, Bartosz Maludzinski\nThis application is going to perform a test comparing our and scikit-learn implementation of linear SVM algorithm.\nWe will measure:\n\tlearning time from 20 points,\n\tpredicting time for 100 points (learning area fitted),\n\ttotal time of running,\n\taccuracy supposing that scikit-learn implementation is perfect.\nThose will be an average of 1000 attempts.\nWait until the end of test...")

# Prepare example dataset for the test
# Learning set X,y
X = np.matrix([[0.13856, 0.5131],
                   [0.25921, 0.26108],
                   [0.90102, 0.58198],
                   [0.02212, 0.7043],
                   [0.2488, 0.9904],
                   [0.22374, 0.56751],
                   [0.86701, 0.18306],
                   [0.00865, 0.45062],
                   [0.04829, 0.68082],
                   [0.24042, 0.8313],
                   [1.09778, 1.235],
                   [1.02459, 1.08723],
                   [1.50164, 1.06381],
                   [1.07053, 1.14818],
                   [1.70556, 1.78613],
                   [1.0746, 1.99822],
                   [1.7923, 1.44644],
                   [1.68212, 1.85668],
                   [1.34193, 1.08593],
                   [1.37754, 1.95356]])

y = np.matrix([[1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [-1],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.]])

y_old = np.ravel(y,order='C');

# Variables to store measured time and accuracy
time_learn_new = []
time_learn_old = []

time_pred_new = []
time_pred_old = []

accuracy = []

# Constants for drawing numbers
num_samples = 100
num_features = 2

for attempt in range(0, 1000):
	# Random set for predictions
	samples = np.matrix(np.random.normal(loc=1.0, size=num_samples * num_features).reshape(num_samples, num_features))
	
	# Turn off console output
	sys.stdout = open(os.devnull, "w")
	
	#Learn scikit-learn algo	
	start = time.clock()
	old = lib.SVC(kernel='linear')
	old.fit(X, y_old)
	elapsed = time.clock() - start
	time_learn_old.append(elapsed)
	
	#Learn our algo	
	start = time.clock()
	new = ours.SVM()
	new.fit(X, y)
	elapsed = time.clock() - start
	time_learn_new.append(elapsed)
	
	# Turn on console output
	sys.stdout = sys.__stdout__
	
	# Predict scikit-learn algo
	start = time.clock()
	old_pred = old.predict(samples)
	elapsed = time.clock() - start
	time_pred_old.append(elapsed)
	
	# Predict our algo
	start = time.clock()
	new_pred = new.predict(samples)
	elapsed = time.clock() - start
	time_pred_new.append(elapsed)
	
	# Prepare data for comparison
	new_pred = np.ravel(new_pred, order='C')
	
	equals = 0
	for i in range(0, num_samples):
		if (old_pred[i] == new_pred[i]):
			equals = equals + 1
	accuracy.append(equals)
	
	print "\t\r%.1f%%\tcompleted" % (attempt/10+1),

if (len(time_learn_new) == len(time_learn_old) == len(time_pred_new) == len(time_pred_old) == len(accuracy) == 1000):
	
	time_learn_old_mean = np.mean(time_learn_old)
	time_learn_new_mean = np.mean(time_learn_new)
	time_learn_ratio = time_learn_new_mean/time_learn_old_mean
	
	time_pred_old_mean = np.mean(time_pred_old)
	time_pred_new_mean = np.mean(time_pred_new)
	time_pred_ratio = time_pred_new_mean/time_pred_old_mean

	time_total_old_mean = np.mean(time_learn_old+time_pred_old)
	time_total_new_mean = np.mean(time_learn_new+time_pred_new)
	time_total_ratio = time_total_new_mean/time_total_old_mean

	print "\n\nTest results:\n"
	print "Average learning time [s]:"
	print "\tScikit-learn:\t\t%f" % (time_learn_old_mean)
	print "\tOur implementation:\t%f" % (time_learn_new_mean)
	print "\t\tratio: %.1fx" % (time_learn_ratio)
	
	print "Average predicting time [s]:"
	print "\tScikit-learn:\t\t%f" % (time_pred_old_mean)
	print "\tOur implementation:\t%f" % (time_pred_new_mean)
	print "\t\tratio: %.1fx" % (time_pred_ratio)
	
	print "Average total time [s]:"
	print "\tScikit-learn:\t\t%f" % (time_total_old_mean)
	print "\tOur implementation:\t%f" % (time_total_new_mean)
	print "\t\tratio: %.1fx" % (time_total_ratio)	
	
	print "Accuracy:"
	print "\tAverage accuracy [%%]:\t%f" % (np.mean(accuracy))
	print "\tStandard deviation:\t%f" % (np.std(accuracy))
	print "\tMin accuracy [%%]:\t%f" % (np.min(accuracy))
	print "\tMax accuracy [%%]:\t%f" % (np.max(accuracy))

else:
	print("\nCritical error in test.")



