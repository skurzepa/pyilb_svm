import sys
sys.path.insert(0, 'pyilb_svm/')
import numpy as np
import svm
import matplotlib.pyplot as plt


class ExampleSVM(object):
    X = np.matrix([[0.13856, 0.5131],
                   [0.25921, 0.26108],
                   [0.90102, 0.58198],
                   [0.02212, 0.7043],
                   [0.2488, 0.9904],
                   [0.22374, 0.56751],
                   [0.86701, 0.18306],
                   [0.00865, 0.45062],
                   [0.04829, 0.68082],
                   [0.24042, 0.8313],
                   [1.09778, 1.235],
                   [1.02459, 1.08723],
                   [1.50164, 1.06381],
                   [1.07053, 1.14818],
                   [1.70556, 1.78613],
                   [1.0746, 1.99822],
                   [1.7923, 1.44644],
                   [1.68212, 1.85668],
                   [1.34193, 1.08593],
                   [1.37754, 1.95356]])

    y = np.matrix([[1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [1.],
                   [-1],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.],
                   [-1.]])

    def example_method(self, num_samples=50, num_features=2, type_of_samples="random"):

        if type_of_samples == "random":
            samples = np.matrix(np.random.normal(size=num_samples * num_features)
                                .reshape(num_samples, num_features))

            labels = 2 * (samples.sum(axis=1) > 0) - 1.0

        else:
            samples = self.X
            labels = self.y

        sv = svm.SVM()
        predictor = sv.fit(samples, labels)

        # plt.scatter(*zip(*np.array(samples)))
        self.plot_Scatter(samples, labels)

        # x_min, x_max = samples[:, 0].min() - 1, samples[:, 0].max() + 1
        # y_min, y_max = samples[:, 1].min() - 1, samples[:, 1].max() + 1

        x_min, x_max = samples[:, 0].min() - 0.1, samples[:, 0].max() + 0.1
        y_min, y_max = samples[:, 1].min() - 0.1, samples[:, 1].max() + 0.1

        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)

        # Calculate y=ax+b function
        w = predictor._w.copy()
        b = predictor._b.copy()

        w = np.squeeze(np.asarray(w))
        b = float(b)


        a = (-1.)*w[0]/w[1]
        b = (-1.)*b/w[1]

        print("A:")
        print(a)

        print("B:")
        print(b)

        x = np.array([x_min,x_max])
        y = a*x+b

        print("X values")
        print(x)
        print("Y values")
        print(y)

        plt.plot(x,y,'r-')




    def plot_Scatter(self, X, y):
        true_false_mask = np.squeeze(np.asarray(y > 0))

        X1 = X[true_false_mask]
        # y1 = y[true_false_mask]

        true_false_mask = np.logical_not(true_false_mask)

        X2 = X[true_false_mask]
        # y2 = y[true_false_mask]

        plt.scatter(*zip(*np.array(X1)), color='blue')
        plt.scatter(*zip(*np.array(X2)), color='red')

        # plt.scatter(flatten(X[:, 0]), flatten(X[:, 1]), c=flatten(y) )


def flatten(m):
    return np.array(m).reshape(-1, )


def example(num_samples=200, num_features=2):
    ex = ExampleSVM()
    ex.example_method(num_samples, num_features, "random")
    plt.show()


if __name__ == "__main__":
    example()
