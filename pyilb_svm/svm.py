import trainer as tr
import kernel as kl


class SVM(object):

    def fit(self, X, y, kernel=kl.Kernel.linear()):
        self._predictor = tr.Trainer(kernel).train(X,y)
        return self._predictor

    def predict(self, x):
        return self._predictor.predict(x)
